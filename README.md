# odin-recipes

This is a practice project from the Odin Project (TOP) curriculum. It is meant to develop skills and experience with writing HTML documents.

The assignment may be found here: https://www.theodinproject.com/lessons/foundations-recipes
